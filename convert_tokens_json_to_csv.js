const commandLineArgs = require("command-line-args")
const fs = require("fs")

const optionsDefinition = [{ defaultOption: true, name: "tokens", type: String }]
const options = commandLineArgs(optionsDefinition)

const tokens = JSON.parse(fs.readFileSync(options.tokens, { encoding: "utf-8" }))
for (let token of tokens) {
  console.log(token)
}
