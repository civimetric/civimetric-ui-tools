const commandLineArgs = require("command-line-args")
const fs = require("fs")

function generateRespondantsTokens(respondantsFilePath) {
  if (!fs.existsSync(respondantsFilePath)) {
    return [null, `File "${respondantsFilePath}" doesn't exist.`]
  }
  if (!fs.lstatSync(respondantsFilePath).isFile()) {
    return [null, `Node at "${respondantsFilePath}" is not a file.`]
  }
  let respondants = JSON.parse(fs.readFileSync(respondantsFilePath, { encoding: "utf-8" }))
  if (respondants === null || respondants === undefined) {
    return [respondants, "Missing respondants"]
  }
  if (!Array.isArray(respondants)) {
    return [respondants, `Expected an array got "${typeof respondants}"`]
  }
  const errors = {}
  respondants = [...respondants]
  for (let [index, respondant] of respondants.entries()) {
    const [generatedRespondant, error] = generateRespondantToken(respondant)
    respondants[index] = generatedRespondant
    if (error !== null) {
      errors[index] = error
    }
  }
  if (Object.keys(errors).length > 0) {
    return [respondants, errors]
  }
  fs.writeFileSync(respondantsFilePath, JSON.stringify(respondants, null, 2))
  return [respondants, null]
}

function generateRespondantToken(respondant) {
  if (respondant === null || respondant === undefined) {
    return [respondant, "Missing value"]
  }
  if (typeof respondant !== "object") {
    return [respondant, `Expected an object got "${typeof respondant}"`]
  }

  const errors = {}
  const remainingKeys = new Set(Object.keys(respondant))
  respondant = { ...respondant }

  for (let key of ["email", "name"]) {
    remainingKeys.delete(key)
    const [value, error] = validateMayBeTrimmedString(respondant[key])
    respondant[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }
  if (respondant.email === null && respondant.name === null) {
    errors.email = errors.name = 'Missing "email" or "name"'
  }

  {
    const key = "token"
    remainingKeys.delete(key)
    let [value, error] = validateMayBeTrimmedString(respondant[key])
    if (error === null) {
      if (value === null) {
        value = generateToken()
      }
    } else {
      errors[key] = error
    }
    respondant[key] = value
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [respondant, Object.keys(errors).length === 0 ? null : errors]
}

function generateToken() {
  // Cf https://stackoverflow.com/questions/6248666/how-to-generate-short-uid-like-ax4j9z-in-js
  // Generate the UID from two parts here to ensure the random number provide enough bits.
  var firstPart = (Math.random() * 46656) | 0
  var secondPart = (Math.random() * 46656) | 0
  firstPart = ("000" + firstPart.toString(36)).slice(-3)
  secondPart = ("000" + secondPart.toString(36)).slice(-3)
  return firstPart + secondPart
}

function validateMayBeTrimmedString(value) {
  if (value === null || value === undefined) {
    return [null, null]
  }
  if (typeof value !== "string") {
    return [value, `Expected a string got "${typeof value}"`]
  }
  value = value.trim()
  if (!value) {
    return [null, null]
  }
  return [value, null]
}

const optionsDefinition = [{ defaultOption: true, name: "respondants", type: String }]
const options = commandLineArgs(optionsDefinition)

const [respondants, error] = generateRespondantsTokens(options.respondants)
if (error !== null) {
  console.log(error)
  process.exit(1)
}
process.stdout.write(JSON.stringify(respondants.map(respondant => respondant.token).sort(), null, 2))
