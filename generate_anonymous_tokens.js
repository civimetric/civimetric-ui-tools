const commandLineArgs = require("command-line-args")

function generateToken() {
  // Cf https://stackoverflow.com/questions/6248666/how-to-generate-short-uid-like-ax4j9z-in-js
  // Generate the UID from two parts here to ensure the random number provide enough bits.
  var firstPart = (Math.random() * 46656) | 0
  var secondPart = (Math.random() * 46656) | 0
  firstPart = ("000" + firstPart.toString(36)).slice(-3)
  secondPart = ("000" + secondPart.toString(36)).slice(-3)
  return firstPart + secondPart
}

const optionsDefinition = [{ defaultOption: true, name: "count", type: Number }]
const options = commandLineArgs(optionsDefinition)

const tokens = [...Array(options.count).keys()].map(generateToken)
process.stdout.write(JSON.stringify(tokens.sort(), null, 2))
