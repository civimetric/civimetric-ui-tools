# Civimetric-UI-Tools

_External tools for [Civimetric-UI](https://framagit.org/civimetric/civimetric-ui) that must not be deployed on the same server as Civimetric-UI_

These tools allow to:
* Generate anonymous tokens for surveys when the respondants are a closed list of persons.

## Installation

```bash
npm install
```

## Execution

```bash
node generate_respondants_tokens.js respondants.json > tokens.json
```
